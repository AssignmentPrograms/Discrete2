
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

enum
{
    INFIX,    // = 0
    PREFIX,   // = 1
    POSTFIX   // = 2
};

int Calculate( FILE* fp );

int main( int argc, char** argv )
{
    char* file = argv[1];

    if( !file )
    {
        printf("No input file!\n");
        exit(1);
    }

    FILE* fp = fopen( file, "r" );
    if( !fp )
    {
        printf("Invalid input file!\n");
        exit(1);
    }

    int endOfFile = 0;

    while( !endOfFile )
    {
        endOfFile = Calculate(fp);
    }

    fclose( fp );

    return 0;
}


static inline double Simplify( double* operands, char op )
{
    double result;

    double leftOperand = operands[0];
    double rightOperand = operands[1];

    switch( op )
    {
        case '+':
            result = leftOperand + rightOperand;
            break;

        case '-':
            result = leftOperand - rightOperand;
            break;

        case '*':
            result = leftOperand * rightOperand;
            break;

        case '/':
            result = leftOperand / rightOperand;
            break;

        default:
            printf("Error: %c is not a valid token\n", op);
            exit(1);
    }


    return result;
}

static inline void PrintArray( char* formula, int length )
{
    for(int i = 0; i < length; i++)
    {
        printf("%c ", formula[i]);
    }

    printf("\n");
}

static inline double FindAnswer( double* numArray, int numArrayLength, char* formula, int formulaLength, int type )
{
    double result;

    int solved = 0, consecDigit = 0;
    double operands[2]; // Left operand = [0], Right operand = [1]
    char op;

    int newValLoc, diff = 0;

    int simplified = 0;

    int prevLength;

    int readyPostFix = 1;
    int numOps = 0;

    while( !solved )
    {
        if( type == POSTFIX )
            readyPostFix = 0;

        // Iterate over the array
        prevLength = formulaLength;
        for( int i = 0; i < prevLength; i++ )
        {
            // Simplify once per traversal
            if( !simplified )
            {
                // '_' is currently being used to represent a simplified spot in our formula whose result is in numArray
                if( isdigit( formula[i] ) || formula[i] == '_' )
                {
                    // TODO: This sometimes crashes here... And sometimes doesn't (for postfix only.)
                    //if( type != POSTFIX )
                        operands[consecDigit] = numArray[i - numOps];
                    if( consecDigit < 2 )
                        consecDigit++;
                }
                else
                {
                    numOps++;
                    op = formula[i];

                    readyPostFix = 1;

                    if( type == PREFIX )
                        consecDigit = 0;
                }

                if( type == POSTFIX && consecDigit >= 2 && readyPostFix)
                {
                    // Grab the last 2 digits
                    double LHS = numArray[i - numOps - 1]; // Left operand
                    double RHS = numArray[i - numOps]; // Right operand
                    operands[0] = LHS;
                    operands[1] = RHS;
                }

                if( consecDigit >= 2 && readyPostFix )
                {
                    newValLoc = i - 2;
                    double val = Simplify( operands, op );
                    formula[newValLoc] = '_'; // We have simplified at this location, there is a digit in numArray for this
                    // newValLoc+1 because we don't have an operator here. -numOps because i takes into account operators
                    int index = newValLoc + 1 - numOps;
                    numArray[index] = val;

                    simplified = 1;
                    // Length of our numArray is 1 less since we have simplified 2 numbers into 1
                    numArrayLength--;
                    formulaLength -= 2;

                    consecDigit = 0;
                }
            }
            else
            {
                // We are still continuing through the array, however we have simplified, so time to shift things down
                // If we are simplified then move everything remaining to the left
                diff++;
                // diff amount more than where our simplification took place
                if( (newValLoc + diff) <= formulaLength )
                    formula[newValLoc + diff] = formula[i];

                //PrintArray( formula, formulaLength );

                if( i - numOps <= numArrayLength )
                    numArray[((newValLoc + 1) - numOps) + diff] = numArray[i - numOps];
            }
        }

        // length=1 means we have simplified as much as we can... The answer is all that remains
        if( formulaLength < 2 )
        {
            result = numArray[0];
            solved = 1;
        }
        else
        {
            // Our array size shrunk, let's get rid of the slack, we have already shifted everything over so no worries
            formula = (char*) realloc( formula, sizeof(char) * formulaLength );
            numArray = (double*) realloc( numArray, sizeof(double) * numArrayLength );

            if( !formula )
            {
                printf("Formula failed to allocate memory!\n");
                exit(1);
            }

            if( !numArray )
            {
                printf("numArray failed to allocate memory!\n");
                exit(1);
            }
        }

        simplified = 0;
        numOps = 0;
        diff = 0;
    }

    printf("%f\n", result);

    return result;
}

int Calculate( FILE* fp )
{
    int type;
    int endOfFile;

    char* formula = (char*) malloc( sizeof(char) * 2 );
    double* numArray = (double*) malloc( sizeof(double) * 2 );

    if( !formula )
    {
        printf("Formula failed to allocate memory!\n");
        exit(1);
    }

    if( !numArray )
    {
        printf("numArray failed to allocate memory!\n");
        exit(1);
    }

    char val;
    int totSize = 0; // Track size of our formula without spaces

    int flag = 1;
    int numValues = 0;
    int numOps = 0;
    int newNum = 0; // TODO: Rename? Meant to represent when we have hit a number for first time (e.g. 12... 1 is new, 2 is sameNum)

    int ready = 1;

    double number = 0.0;

    int read;
    while( ( ( read = fgetc(fp) ) != '\n' ) && read != EOF )
    {
        val = (char) read;

        if( flag )
        {
            flag = 0;

            if( isdigit(val) )
                type = POSTFIX;
            else if( !isspace(val) )
                type = PREFIX;
            else
                flag = 1; // Was nothing of interest, TRY AGAIN
        }

        if( isdigit(val) )
        {
            if( !newNum )
            {
                newNum = 1;
                numValues++;
            }
            else
            {
                number *= 10;
                ready = 0;
            }

            number += val - '0';
        }
        else
        {
            if( !isspace(val) )
                numOps++; // Safe to assume val is an operator

            if( number > 0 ) // TODO: Allow 0 to be in input formula (fails if 0 is not last character)
            {
                if( numValues == 0 )
                    printf("numValues is 0, can't realloc!??\n");
                numArray = (double*) realloc( numArray, sizeof(double) * numValues );

                if( !numArray )
                {
                    printf("Allocation of numArray during initial read failed!\n");
                    exit(1);
                }

                numArray[numValues - 1] = number;
            }

            number = 0;
            newNum = 0;
        }

        if( !isspace(val) && ready )
        {
            totSize++;
            formula = (char*) realloc( formula, sizeof(char) * totSize );
            if( number < 10 && number >= 0 ) // If we have something that can be represented with 1 character
                formula[totSize - 1] = val;
            else
                formula[totSize - 1] = '_'; // Place holder that represents a number value
        }
        if( !isspace(val) )
        {
            if( !isdigit( ( val = fgetc(fp) ) ) && !ready && number > 0)
            {
                formula[totSize - 1] = '_'; // Place holder that represents a number value
            }
            ungetc(val, fp);
        }

        ready = 1;
    }

    if( number > 0 ) // TODO: Allow 0 to be in input formula (fails if 0 is not last character)
    {
        if( numValues == 0 )
            printf("numValues is 0, can't realloc!??\n");
        numArray = (double*) realloc( numArray, sizeof(double) * numValues );

        if( !numArray )
        {
            printf("Allocation of numArray during initial read failed!\n");
            exit(1);
        }

        numArray[numValues - 1] = number;
    }

    if( read == EOF )
        endOfFile = 1;
    else
        endOfFile = 0;

    if( !endOfFile )
        FindAnswer( numArray, numValues, formula, totSize, type );

    return endOfFile;
}
