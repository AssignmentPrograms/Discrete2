/*
Name: Brandon Jones
WSUID: N534H699

THIS PROGRAM IS FOR QUESTION 10

User Note:
I did both reflexive and symmetric.
Refexive is the output by default.
Uncomment the "SymmetricClosure(...)" function call line and comment "ReflexiveClosure(...) function call line to see the symmetric closure (if needed).
*/


#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"

void GetMatrix( FILE* fp, int** matrix, int length );
void AdjustArray( char* array, int index, int length );
void ReflexiveClosure( int** matrix, int length );
void SymmetricClosure( int** matrix, int length );
void PrintMatrixFormated( int** matrix, int length );
void PrintMatrix( int** matrix, int length );
int GetMatrixDimensions( FILE* fp );


int main(int argc, char** argv)
{
    FILE* fp = fopen( argv[1], "r" );

    if( fp == NULL )
    {
        printf("Error opening file!\n");
        exit(EXIT_FAILURE);
    }

    int matrixLength = GetMatrixDimensions(fp);

    int** matrix;

    matrix = (int**) malloc( sizeof(int*) * matrixLength );

    for( int i = 0; i < matrixLength; i++ )
    {
        matrix[i] = (int*) malloc( sizeof(int) * matrixLength );
    }

    GetMatrix( fp, matrix, matrixLength );

    //printf("Original Matrix:\n");
    //PrintMatrix( matrix, matrixLength );

    //printf("\n\nReflexive Closure:\n");
    ReflexiveClosure( matrix, matrixLength );

    //PrintMatrix( matrix, matrixLength );

    //printf("\n\nSymmetric Closure:\n");
    //SymmetricClosure( matrix, matrixLength );

    //PrintMatrix( matrix, matrixLength );

    //printf("\nFormatted:\n\n");

    PrintMatrixFormated( matrix, matrixLength );

    fclose(fp);
}

void PrintMatrixFormated( int** matrix, int length )
{
    printf("{");
    for( int i = 0; i < length; i++ )
    {
        printf(" {");
        for( int j = 0; j < length; j++ )
        {
            printf("%d", matrix[i][j]);
            if( j < length - 1 )
                printf(", ");
        }
        printf("}");

        if( i < length - 1 )
            printf(",");
    }
    printf(" }\n");
}

void PrintMatrix( int** matrix, int length )
{
    for( int i = 0; i < length; i++ )
    {
        for( int j = 0; j < length; j++ )
        {
            printf("%d ", matrix[i][j]);
        }
        printf("\n");
    }
}

void AdjustArray( char* array, int index, int length )
{
    for( int i = index; i < length - 1; i++ )
        array[i] = array[i + 1];

    char* temp = realloc( array, (length - 1) * sizeof(char) );

    if( temp == NULL && length > 1 )
    {
        exit(EXIT_FAILURE);
    }

    // TODO: BAD BAD BAD
    array = temp;
}


void ReflexiveClosure( int** matrix, int length )
{
    int j;

    for( int i = 0; i < length; i++ )
    {
        j = i;
        if( !matrix[i][j] )
        {
            matrix[i][j] = 1;
        }
    }
}


void SymmetricClosure( int** matrix, int length )
{
    for( int i = 0; i < length; i++ )
    {
        for( int j = 0; j < i; j++ )
        {
            if( matrix[i][j] != matrix[j][i] )
                if( !matrix[i][j] )
                    matrix[i][j] = 1;
                else
                    matrix[j][i] = 1;
        }
    }
}


void ReadMatrix( FILE* fp, int** matrix, int length )
{
    char val;
    int row = 0;
    int col = 0;

    while( ( val = fgetc(fp) ) != '\n' && !feof(fp) )
    {
        if( isdigit( val ) )
        {
            if( row < length )
            {
                matrix[row][col] = val - '0';

                if( col < length - 1 )
                {
                    col++;
                }
                else
                {
                    row++;
                    col = 0;
                }
            }
        }
    }
}

void EatLine( FILE* fp )
{
    char val;
    while( ( val = fgetc(fp) ) != '\n' && !feof(fp) )
    {
        // nom nom
    }
}

int GetMatrixDimensions( FILE* fp )
{
    int result = 0;

    char val;
    EatLine(fp);

    while( ( val = fgetc(fp) ) != '\n' && !feof(fp) )
    {
        if( isdigit(val) )
            result++;
        else if( val == '}' )
            break;
    }

    fseek( fp, 0, SEEK_SET );

    return result;
}

void GetMatrix( FILE* fp, int** matrix, int length )
{
    char val;
    int flag = 1;
    while( !feof(fp) )
    {
        // If first line
        if( flag )
        {
            EatLine(fp);
            flag = 0;
        }
        else
            ReadMatrix( fp, matrix, length );
    }
}


/*
- A plaintext file.
- The first line completely describes a finite set.
- The second line contains a 0-1 matrix in the format { {row}, {row}, ... , {row} }
- You can assume that if you have n elements on the first row, that the matrix will be a well-defined n x n matrix.

Example:

a, b, 1, 8, 94, monkey, 47, c, 5
{ {0, 1, 1, 0, 1, 0, 1, 1, 1}, {0, 1, 1, 1, 1, 1, 1, 0, 0},  [there are 7 more of these for a total of 9 rows] }

Output:
Output
- To stdout: An n x n  0-1 matrix in the formatted as above { {row}, {row}, ... , {row} }
*/
