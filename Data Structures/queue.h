

#ifndef QUEUE_H
#define QUEUE_H

#include <stdlib.h>
#include <stdio.h>



#ifndef TYPE
#define TYPE
    typedef enum TypeEnum
    {
        NOTHING,
        INT,
        REAL,
        CHAR
    } Type;
#endif

typedef struct QueueStruct
{
    union
    {
        int* front_i;
        double* front_r;
        char* front_c;
    };

    union
    {
        int* tail_i;
        double* tail_r;
        char* tail_c;
    };

    int size;

    Type type;

} Queue;


Queue CreateQueue( Type type )
{
    Queue queue;

    queue.size = 0;

    queue.type = type;

    switch( queue.type )
    {
        case INT:
        {
            queue.front_i = (int*) malloc( 0 );
            queue.tail_i = queue.front_i;
        } break;

        case REAL:
        {
            queue.front_r = (double*) malloc( 0 );
            queue.tail_r = queue.front_r;
        }

        case CHAR:
        {
            queue.front_c = (char*) malloc( 0 );
            queue.tail_c = queue.front_c;
        } break;

        default:
        {
            printf("Not a supported queue type!\n");
            exit(1);
        }
    }

    return queue;
}

void DeleteQueue( Queue* queue )
{
    switch( queue->type )
    {
        case INT:
        {
            free( queue->front_i );
        } break;

        case REAL:
        {
            free( queue->front_r );
        } break;

        case CHAR:
        {
            free( queue->front_c );
        } break;
    }

    queue->size = 0;
    queue->type = NOTHING;
}


void Insert_i( Queue* queue, int val )
{
    queue->size++;
    queue->front_i = (int*) realloc( queue->front_i, sizeof(int) * queue->size );

    queue->tail_i = queue->front_i + (queue->size - 1);

    if( queue->size - 1 == 0 )
        *(queue->front_i) = val;
    else
        *(queue->tail_i) = val;
}

void Insert_r( Queue* queue, double val )
{
    queue->size++;
    queue->front_r = (double*) realloc( queue->front_r, sizeof(double) * queue->size );

    queue->tail_r = queue->front_r + (queue->size - 1);

    if( queue->size - 1 == 0 )
        *(queue->front_r) = val;
    else
        *(queue->tail_r) = val;
}

void Insert_c( Queue* queue, char val )
{
    queue->size++;
    queue->front_c = (char*) realloc( queue->front_c, sizeof(char) * queue->size );

    queue->tail_c = queue->front_c + (queue->size - 1);

    if( queue->size - 1 == 0 )
        *(queue->front_c) = val;
    else
        *(queue->tail_c) = val;
}

static inline void MoveLineBack( Queue* queue )
{
    // TODO: if/switch for queue type and make appropriate type for helper??

    int lastLineSpot = queue->size - 1;

    switch( queue->type )
    {
        case INT:
        {
            int* helper;
            for( int i = 0; i < lastLineSpot; i++ )
            {
                helper = queue->front_i + (lastLineSpot - i);
                *(helper) = *(helper - 1);
            }
        } break;

        case REAL:
        {
            double* helper;
            for( int i = 0; i < lastLineSpot; i++ )
            {
                helper = queue->front_r + (lastLineSpot - i);
                *(helper) = *(helper - 1);
            }
        } break;

        case CHAR:
        {
            char* helper;
            for( int i = 0; i < lastLineSpot; i++ )
            {
                helper = queue->front_c + (lastLineSpot - i);
                *(helper) = *(helper - 1);
            }
        } break;
    }
}

void CutLineInsert_i( Queue* queue, int val )
{
    queue->size++;
    queue->front_i = (int*) realloc( queue->front_i, sizeof(int) * queue->size );

    if( queue->size > 1 )
        MoveLineBack( queue );

    queue->tail_i = queue->front_i + (queue->size - 1);

    *(queue->front_i) = val;
}

void CutLineInsert_r( Queue* queue, double val )
{
    queue->size++;
    queue->front_r = (double*) realloc( queue->front_r, sizeof(double) * queue->size );

    if( queue->size > 1 )
        MoveLineBack( queue );

    queue->tail_r = queue->front_r + (queue->size - 1);

    *(queue->front_r) = val;
}

void CutLineInsert_c( Queue* queue, char val )
{
    queue->size++;
    queue->front_c = (char*) realloc( queue->front_c, sizeof(char) * queue->size );

    if( queue->size > 1 )
        MoveLineBack( queue );

    queue->tail_c = queue->front_c + (queue->size - 1);

    *(queue->front_c) = val;
}


// NOTE: Needed to avoid queue->front++, since realloc requires first arg to be something returned by mallloc...
//queue->front++ is an address that was returned by malloc -> THUS realloc returns NULL
static inline void MoveLineUp( Queue* queue )
{
    // TODO: if/switch for queue type and make appropriate type for helper??

    queue->size--;

    switch( queue->type )
    {
        case INT:
        {
            int* helper;
            for( int i = 0; i < queue->size; i++ )
            {
                helper = queue->front_i + i;
                *(helper) = *(helper + 1);
            }
        } break;

        case REAL:
        {
            double* helper;
            for( int i = 0; i < queue->size; i++ )
            {
                helper = queue->front_r + i;
                *(helper) = *(helper + 1);
            }
        } break;

        case CHAR:
        {
            char* helper;
            for( int i = 0; i < queue->size; i++ )
            {
                helper = queue->front_c + i;
                *(helper) = *(helper + 1);
            }
        } break;
    }
}

int Remove_i( Queue* queue )
{
    int result = *(queue->front_i);

    MoveLineUp( queue );

    queue->front_i = (int*) realloc( queue->front_i, sizeof(int) * queue->size );
    queue->tail_i = queue->front_i + (queue->size - 1);

    return result;
}

double Remove_r( Queue* queue )
{
    double result = *(queue->front_r);

    MoveLineUp( queue );

    queue->front_r = (double*) realloc( queue->front_r, sizeof(double) * queue->size );
    queue->tail_r = queue->front_r + (queue->size - 1);

    return result;
}

char Remove_c( Queue* queue )
{
    char result = *(queue->front_c);

    MoveLineUp( queue );

    queue->front_c = (char*) realloc( queue->front_c, sizeof(char) * queue->size );
    queue->tail_c = queue->front_c + (queue->size - 1);

    return result;
}

int RemoveLast_i( Queue* queue )
{
    int result = *(queue->tail_i);

    queue->size--;

    queue->front_i = (int*) realloc( queue->front_i, sizeof(int) * queue->size );
    queue->tail_i = queue->front_i + (queue->size - 1);

    return result;
}

double RemoveLast_r( Queue* queue )
{
    double result = *(queue->tail_r);

    queue->size--;

    queue->front_r = (double*) realloc( queue->front_r, sizeof(double) * queue->size );
    queue->tail_r = queue->front_r + (queue->size - 1);

    return result;
}

char RemoveLast_c( Queue* queue )
{
    char result = *(queue->tail_c);

    queue->size--;

    queue->front_c = (char*) realloc( queue->front_c, sizeof(char) * queue->size );
    queue->tail_c = queue->front_c + (queue->size - 1);

    return result;
}


void QueuePeek( Queue* queue )
{
    switch( queue->type )
    {
        case INT:
        {
            int result = Remove_i(queue);
            CutLineInsert_i( queue, result );
            printf("Front of Queue: %i\n", result);
        } break;

        case REAL:
        {
            double result = Remove_r(queue);
            CutLineInsert_r( queue, result );
            printf("Front of Queue: %f\n", result);
        } break;

        case CHAR:
        {
            char result = Remove_c(queue);
            CutLineInsert_c( queue, result );
            printf("Front of Queue: %c\n", result);
        } break;
    }
}



// TODO: I rethought this halfway thorugh... So finish if desired... :D
void PrintQueue( Queue* queue )
{
    int size = queue->size;

    for( int i = 0; i < size; i++ )
    {
        switch( queue->type )
        {
            case INT:
                printf("%i ", Remove_i(queue) );
                break;

            case REAL:
                printf("%f ", Remove_r(queue) );
                break;

            case CHAR:
                printf("%c ", Remove_c(queue) );
                break;
        }
    }

    printf("\n");
}










#endif //QUEUE_H
