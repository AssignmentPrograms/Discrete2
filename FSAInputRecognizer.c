#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>

void EvaluateInputOnFSA( FILE* fp );

int numParams = 3;

// TODO: This breaks when you have trailing white space (a space) after the parentheses on any line of the file.

int main( int argc, char** argv )
{
    char* file = argv[1];

    if( !file )
    {
        printf("I expect an input file!\n");
        exit(1);
    }

    FILE* fp = fopen( file, "r" );

    if( !fp )
    {
        printf("File failed to open!");
        exit(1);
    }

    EvaluateInputOnFSA( fp );


    fclose(fp);

    return 0;
}

// Inline function to get the input string from the input file
static inline int GetInputString( FILE* fp, char** result )
{
    int read, size = 1;
    char val;
    while( (read = fgetc(fp)) != EOF && read != '\n' )
    {
        val = (char) read;

        if( isalpha(val) || isdigit(val) )
        {
            (*result)[size - 1] = val;
            *result = realloc( *result, sizeof(char) * (++size) );
        }
    }

    return --size;
}

// Inline function to get the final states form the input file
static inline int GetFinalStates( FILE* fp, char*** result )
{
    int read, numStates = 0, firstRead = 1, numChars = 1;
    char val;

    while( (read = fgetc(fp)) != EOF && read != '\n' )
    {
        val = (char) read;

        if( isalpha(val) || isdigit(val) )
        {
            if( firstRead )
            {
                (*result)[numStates] = malloc( sizeof(char) );

                if( !(*result)[numStates] )
                {
                    printf("Error allocating memory for result[%i]!\n", numStates);
                    exit(1);
                }
            }
            firstRead = 0;

            (*result)[numStates][numChars - 1] = val;

            (*result)[numStates] = realloc( (*result)[numStates], sizeof(char) * (++numChars) );

            if( !(*result)[numStates] )
            {
                printf("Error reallocating memory for result[%i]!\n", numStates);
                exit(1);
            }

        }
        else if( val == ',' )
        {
            (*result)[numStates][numChars - 1] = '\0';

            numStates++;

            (*result) = realloc( *result, sizeof(char*) * (numStates + 1) );

            if( !*result )
            {
                printf("Failed to allocate memory for result in isspace!\n");
                exit(1);
            }

            firstRead = 1;
            numChars = 1;
        }
    }

    (*result)[numStates][numChars - 1] = '\0';

    return numStates+1;
}

static inline int GetOrderedTuples( FILE* fp, char**** result )
{
    int read, numTuples = 0, firstRead = 1, numChars = 1;
    int tupleLoc = 0, flag = 0;
    char val;

    while( (read = fgetc(fp)) != EOF )
    {
        val = (char) read;

        if( val != '\n' )
        {
            if( firstRead )
            {
                (*result)[numTuples] = malloc( sizeof(char*) * numParams );

                if( !(*result)[numTuples] )
                {
                    printf("Error allocating memory for result[%i]!\n", numTuples);
                    exit(1);
                }

                // Allocate space for each of our tuple entries
                for( int i = 0; i < numParams; i++ )
                {
                    (*result)[numTuples][i] = malloc( sizeof(char) );

                    if( !(*result)[numTuples][i] )
                    {
                        printf("Error allocating memory for result[%i][%i]!\n", numTuples, i);
                        exit(1);
                    }
                }
            }
            firstRead = 0;

            // TODO: Should I allow for input character length more than 1?
            if( isalpha(val) || ( flag && isdigit(val) ) )
            {
                flag = 0;

                (*result)[numTuples][tupleLoc][numChars - 1] = val;
                (*result)[numTuples][tupleLoc] = realloc( (*result)[numTuples][tupleLoc], sizeof(char) * (++numChars) );

                if( !(*result)[numTuples][tupleLoc] )
                {
                    printf("Error reallocating result[%i][%i] in isalpha!\n", numTuples, tupleLoc);
                    exit(1);
                }

                //if( !flag )
                    tupleLoc++;
            }
            else if( isdigit(val) )
            {
                (*result)[numTuples][tupleLoc - 1][numChars - 1] = val;

                (*result)[numTuples][tupleLoc - 1] = realloc( (*result)[numTuples][tupleLoc - 1], sizeof(char) * (++numChars) );

                if( !(*result)[numTuples][tupleLoc] )
                {
                    printf("Error reallocating result[%i][%i] in isdigit!\n", numTuples, tupleLoc);
                    exit(1);
                }
            }
            else if( val != '(' && val != ')' )
            {
                if( tupleLoc == 2 )
                    flag = 1;

                (*result)[numTuples][tupleLoc - 1][numChars + 1] = '\0';
                numChars = 1;
            }
        }
        else
        {
            (*result)[numTuples][tupleLoc - 1][numChars - 1] = '\0';

            (*result) = realloc( *result, sizeof(char**) * (++numTuples + 1) );

            if( !(*result) )
            {
                printf("Failed reallocating memory for result!\n");
                exit(1);
            }

            firstRead = 1;
            numChars = 1;
            tupleLoc = 0;
        }
    }

    // We slightly over allocated. And we are civilized programmers, so give the memory back.
    (*result) = realloc( *result, sizeof(char**) * numTuples);

    return numTuples;
}

// I want my states to be integers, no need to keep the letter character
static inline int TranslateStates( char*** FSAtuples, int numTuples, int** out )
{
    int** result = malloc( sizeof(int*) );

    if( !result )
    {
        printf("Error allocating memory for result!\n");
        exit(1);
    }

    (*result) = malloc( sizeof(int) );

    if( !(*result) )
    {
        printf("Error allocating memory for *result!\n");
        exit(1);
    }

    // Exploiting the fact that char* only applies to immediate variable -> currChar is a char only
    char* val, currChar;
    int numInts = 0, index = 0, alreadyHave = 0;
    int firstTime = 1, number = 0;

    // Check both initial states and next states
    for( int j = 0; j < 2; j++ )
    {
        // Go through each tuple, check both the initial state and the final state, grab the corresponding integer (if new)
        for( int i = 0; i < numTuples; i++, index = 0 )
        {
            val = FSAtuples[i][j];

            // Grab the whole number
            while( (val[index]) != '\0' )
                if( isdigit(val[index++]) )
                    // Grab the number this represents
                    number = (number * 10) + (val[index - 1] - '0');

            // Find out if we already have this state
            for( int i = 0; i < numInts; i++ )
                if( (*result)[i] == number )
                    alreadyHave = 1;

            // If we don't already have the integer in our array, then add it
            if( !alreadyHave )
            {
                (*result) = realloc( (*result), sizeof(int) * (++numInts + 1) );

                if( !(*result) )
                {
                    printf("Error allocating memory for result!\n");
                    exit(1);
                }

                // Store the value
                (*result)[numInts - 1] = number;
            }

            alreadyHave = 0;
            number = 0;
        }
    }

    // Populate the out parameter
    (*out) = realloc( *out, sizeof(int) * numInts );
    for( int i = 0; i < numInts; i++ )
        (*out)[i] = (*result)[i];

    free( *result );

    free( result );

    return numInts;
}

// NOTE -> Potential Call: FindNextState( &state, inputString, inputStringSize, 0, FSAtuples, numTuples );
// Function to determine what the new state of our FSA will be
static inline int FindNextState( char** state, char* input, int inputLength, int index, char*** FSAtuples, int numTuples,
                             char** finalStates, int finalStatesSize )
{
    // Are we at the end?
    if( index >= inputLength )
    {
        // Did we end on a final state?
        for( int i = 0; i < finalStatesSize; i++ )
            if( !strcmp((*state), finalStates[i]) )
                return 1;

        // We are not at a final state. This string is not recognized.
        return 0;
    }

    for( int i = 0; i < numTuples; i++ )
    {
        // Are we at the current state?
        if( !strcmp(FSAtuples[i][0], (*state)) )
        {
            // TODO: Need to allow for more than a single character?
            if( FSAtuples[i][2][0] == input[index] )
            {
                // Update our state to be the new state
                (*state) = FSAtuples[i][1];

                // If we have a valid direction then continue on with new state and next character in input string
                // Pass in the new state. Pass in the new input string index
                if( FindNextState( state, input, inputLength, (index + 1), FSAtuples, numTuples, finalStates, finalStatesSize ) )
                    return 1;
                else
                    // Reset our state, keep iterating through tuples, seek another tuple with same current state
                    (*state) = FSAtuples[i][0];
            }
        }
    }

    return 0;
}

// Function to read from input file and initialize table data
void EvaluateInputOnFSA( FILE* fp )
{
    char* inputString = malloc( sizeof(char) );

    if( !inputString )
    {
        printf("Error allocating memory for inputString!\n");
        exit(1);
    }

    int inputStringSize = GetInputString( fp, &inputString );

    char** finalStates = malloc( sizeof(char*) );

    if( !finalStates )
    {
        printf("Error allocating memory for finalStates!\n");
        exit(1);
    }

    (*finalStates) = malloc( sizeof(char) );

    if( !finalStates )
    {
        printf("Error allocating memory for *finalStates!\n");
        exit(1);
    }

    int finalStatesSize = GetFinalStates( fp, &finalStates );

    char*** FSAtuples = malloc( sizeof(char**) );

    if( !FSAtuples )
    {
        printf("Error allocating memory for FSAtuples!\n");
        exit(1);
    }

    int numTuples = GetOrderedTuples( fp, &FSAtuples );

    int* states = malloc( sizeof(int) );

    if( !states )
    {
        printf("Error allocating memory for states!\n");
        exit(1);
    }

    // Get an array of just the numbers of our states, in case we want them
    int numInts = TranslateStates( FSAtuples, numTuples, &states );

    // The state of our machine -> Initial state is s0
    // TODO: Assuming the first parameter of the first tuple is our inital state... Bad idea?
    char* state = FSAtuples[0][0];

    int result = FindNextState( &state, inputString, inputStringSize, 0, FSAtuples, numTuples, finalStates, finalStatesSize );

    if( result )
        printf("yes\n");
    else
        printf("no\n");

    // Free all dat memory
    free( inputString );

    for( int i = 0; i < finalStatesSize; i++ )
        free( finalStates[i] );
    free( finalStates );

    for( int i = 0; i < numTuples; i++ )
    {
        for( int j = 0; j < numParams; j++ )
            free( FSAtuples[i][j] );
        free( FSAtuples[i] );
    }
    free( FSAtuples );

    free( states );
}
