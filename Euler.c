/*
Name: Brandon Jones
WSUID: N534H699

THIS PROGRAM IS FOR #13

User Note:
Currently the input file must not have any blank lines before any text and must have 2 lines of text
The code is ugly and was hacked together out of frustration and hurry...

KNOWN BUG WITH THE FOLLOWING INPUT:
  a, b, c, d, e 
    (a, e); (e,a); (a,c); (c,a); (a,d); (d,a); (a,b); (b,a); (b,d); (d,b); (c, e); (e,c); (b,c); (c,b);
causes it to report Path and then seem to hang for an infinite loop. I just killed it after running 2:04. 
*/

#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"
#include "stack.h"

// TODO: Clean up for scalability and reuse

typedef enum EulerTypeEnum
{
    E_NONE,    // 0
    E_CIRCUIT, // 1
    E_PATH     // 2
} EulerType;

// Directed? MultipleEdges? Loops? -> represented as a bitfield 010 = No, Yes, No
typedef enum GraphTypesEnum
{
    SIMPLE = 0,          // 000
    MULTI = 2,           // 010
    PSEUDO = 3,          // 011
    SIMPLE_DIRECTED = 4, // 100
    MULTI_DIRECTED = 7,  // 111
    MIXED                // defined as 8 (1000) for now...
} GraphTypes;

typedef struct MatrixStruct
{
    int numRows;
    int numCols;

    int** matrix;
} Matrix;

typedef struct GraphStruct
{
    // Implement vertices as an array of characters (representing a set)
    char* vertices;
    int numVertices;

    int* vertexDegree;

    // Edges will be a pointer to pointers to characters, with [0] = initial vertex, [1] = terminal vertex
    char** edges;
    int numEdges;

    // Adjacency matrix for the graph
    Matrix adjacencyMatrix;

    int isUndirected;

    int eulerType;

    // For scalability reasons, allow specific graph type
    GraphTypes type;
} Graph;

void ScanLineForCardinality( int* cardinality, FILE* fp );
void GetCardinalities( int* numVertices, int* numEdges, FILE* fp );
void CreateAdjacencyMatrix( Graph* graph );
void GetVertices( Graph* graph, FILE* fp );
void GetEdges( Graph* graph, FILE* fp );
void GetEuler( Graph* graph );
int TestGraph( Graph* graph );

// Some debugging functions
void PrintMatrix( Matrix* matrix );
void PrintVertices( Graph* graph );
void PrintEdges( Graph* graph );

int main( int argc, char** argv )
{
    FILE* fp = fopen( argv[1], "r" );

    if( fp == NULL )
    {
        printf("Error opening file from command line argument!\n");
        exit(EXIT_FAILURE);
    }

    Graph graph;
    graph.numVertices = 0;
    graph.numEdges = 0;

    GetCardinalities( &graph.numVertices, &graph.numEdges, fp );

    // Initialize values of adjacency matrix rows and columns
    graph.adjacencyMatrix.numRows = graph.adjacencyMatrix.numCols = graph.numVertices;

    // Allocate space for vertice degrees
    graph.vertexDegree = (int*) malloc( sizeof(int) * graph.numVertices );

    // Allocate space for adjacency matrix
    graph.adjacencyMatrix.matrix = (int**) malloc( sizeof(int*) * graph.adjacencyMatrix.numRows );

    for( int i = 0; i < graph.adjacencyMatrix.numRows; i++ )
    {
        graph.adjacencyMatrix.matrix[i] = (int*) malloc( sizeof(int) * graph.adjacencyMatrix.numCols );
    }

    // Allocate space for vertices and edges
    graph.vertices = (char*) malloc( sizeof(char) * graph.numVertices );
    graph.edges = (char**) malloc( sizeof(char*) * graph.numEdges );

    for( int i = 0; i < graph.numEdges; i++ )
    {
        // Each edge consists of 2 vertices, edge[i][0] = initial vertex, edge[i][1] = termianl vertex
        graph.edges[i] = (char*) malloc( sizeof(char) * 2 );
    }

    // TODO: Let the function figure this out....
    //graph.type = SIMPLE_DIRECTED;
    graph.type = SIMPLE; // NOTE: Set to SIMPLE to not treat adjacency matrix as symmetric

    GetVertices( &graph, fp );
    GetEdges( &graph, fp );

    // TODO: CreateAdjacencyMatrix ensure our assumptions are correct
    CreateAdjacencyMatrix( &graph );

    //PrintMatrix( &graph.adjacencyMatrix );

    //PrintVertices( &graph );
    //printf("\n");
    //PrintEdges( &graph );

    switch( TestGraph( &graph ) )
    {
        case E_CIRCUIT:
        {
            printf("Circuit\n");
            GetEuler( &graph );
        } break;

        case E_PATH:
        {
            printf("Path\n");
            GetEuler( &graph );
        } break;

        case E_NONE: // Fall through to default

        Default:
        {
            printf("None");
        } break;
    }

    /*for(int i = 0; i < graph.numVertices; i++)
    {
        printf("Degree of Vertex %i: %i\n", i+1, graph.vertexDegree[i]);
    }*/

    return 0;
}

void PrintVertices( Graph* graph )
{
    for( int i = 0; i < graph->numVertices; i++ )
    {
        printf("%i Vertex: %c\n", i, graph->vertices[i]);
    }
}

void PrintEdges( Graph* graph )
{
    for( int i = 0; i < graph->numEdges; i++ )
    {
        printf("%i Edge: ", i);
        for( int j = 0; j < 2; j++ )
        {
            printf("%c", graph->edges[i][j]);
        }
        printf("\n");
    }
}

void PrintMatrix( Matrix* matrix )
{
    for( int i = 0; i < matrix->numRows; i++ )
    {
        for( int j = 0; j < matrix->numCols; j++ )
        {
            if( j != matrix->numCols - 1 )
                printf("%i ", matrix->matrix[i][j]);
            else
                printf("%i", matrix->matrix[i][j]);
        }
        printf("\n");
    }
}

void ScanLineForCardinality( int* cardinality, FILE* fp )
{
    char val;

    while( ( val = fgetc(fp) ) != '\n' && !feof(fp) )
    {
        if( isalpha(val) )
        {
            (*cardinality) += 1;
        }
    }
}


void GetCardinalities( int* numVertices, int* numEdges, FILE* fp )
{
    for( int i = 0; i < 2; i++ )
    {
        if( !i )
            ScanLineForCardinality( numVertices, fp ); // First Line
        else
            ScanLineForCardinality( numEdges, fp ); // Second Line
    }

    // For each edge we counted 2 vertices, so take the current edge val and div by 2
    *numEdges /= 2;

    // We have found the cardinalities, now lets get this fp back to the beginning
    fseek( fp, 0, SEEK_SET);
}


void GetVertices( Graph* graph, FILE* fp )
{
    char val;
    int i = graph->numVertices;

    while( ( val = fgetc(fp) ) != '\n' && !feof(fp) )
    {
        if( isalpha(val) && i )
        {
            // Lowercase the val just to ensure we have a consistent convention
            val = tolower(val);
            graph->vertices[graph->numVertices - i] = val;
            i--;
        }
    }
}

void GetEdges( Graph* graph, FILE* fp )
{
    char val;
    int i = graph->numEdges; // represents the edge that we are on
    int j = 0; // represents the index of either initial [0] vertex or terminal [1] vertex

    while( ( val = fgetc(fp) ) != '\n' && !feof(fp) )
    {
        if( isalpha(val) && i )
        {
            // Lowercase the val just to ensure we have a consistent convention
            val = tolower(val);
            graph->edges[graph->numEdges - i][j] = val;

            // If j = 1 (not 0), then we are on the terminal vertex,
            //                   thus change i (for new edge) and reset j to indicate intial vertex (j = 0)
            // Else j = 0... then we are only on initial vertix, so increment j (to get to terminal vertex)
            if( j )
            {
                i--;
                j = 0;
            }
            else
                j++;
        }
    }
}

/*
void GetDegree_Undirected( Graph* graph )
{
    for( int i = 0; i < graph->adjacencyMatrix.numRows; i++ )
    {
        for( int j = 0; j < graph->adjacencyMatrix.numCols; j++ )
        {

        }
    }
}
*/


/*void GetGraphType( Graph* graph )
{
    // NOTE: We are assuming Undirected, No Multiple Edges, No Loops
    // Strategy Prove or disprove our assumptions and adjust as necessary
    // Undirected: Prove wrong by showing at least 1 path who is not symmetric
    // No Mult. Edges: Prove wrong by showing at least 1 path repeated
    // No Loops: Prove wrong by showing at least one edge with initial = terminal vertex
    int graphTypeBitfield = 0; // Init to 000

    int coords[2];

    // TODO: Didn't complete the below NOTE yet :D
    // TODO: NOTE: For now will just solve the directed/undirected problem for the current homework problem
    // TODO: The other 2 checks

    for( int i = 0; i < graph->numEdges; i++ )
    {
        for( int j = 0; j < 2; j++ )
        {
            coords[j] = graph->edges[i][j] - 'a';
        }

        if( coords[0] == coords[1] )
        {

        }
    }
}*/

static inline int IsUndirectedByMatrix( Graph* graph )
{
    for( int i = 0; i < graph->adjacencyMatrix.numRows; i++ )
    {
        for( int j = 0; j < graph->adjacencyMatrix.numCols; j++ )
        {
            if( graph->adjacencyMatrix.matrix[i][j] != graph->adjacencyMatrix.matrix[j][i] )
            { return 0; }
        }
    }

    return 1;
}

static inline int HasSymmetricEdge( Graph* graph, char c1, char c2 )
{
    int result = 0;

    for( int i = 0; i < graph->numEdges; i++ )
    {
        if( graph->edges[i][0] == c2 && graph->edges[i][1] == c1 )
        {
            result = 1;
            break;
        }
    }

    return result;
}

static inline int IsUndirectedByEdges( Graph* graph )
{
    int result = 1;
    for( int i = 0; i < graph->numEdges; i++ )
    {
        if( !HasSymmetricEdge(graph, graph->edges[i][0], graph->edges[i][1] ) )
        {
            result = 0;
            break;
        }
    }

    return result;
}

void CreateAdjacencyMatrix( Graph* graph )
{
    graph->isUndirected = IsUndirectedByEdges( graph );

    // Intialize the adjacency matrix;
    for( int i = 0; i < graph->adjacencyMatrix.numRows; i++ )
    {
        graph->vertexDegree[i] = 0;
        for( int j = 0; j < graph->adjacencyMatrix.numCols; j++ )
        {
            graph->adjacencyMatrix.matrix[i][j] = 0;
        }
    }

    // Store the row/col representation of the edge ([0] = row, [1] = col)
    int coords[2];

    for( int i = 0; i < graph->numEdges; i++ )
    {
        for( int j = 0; j < 2; j++ )
        {
            coords[j] = graph->edges[i][j] - 'a';
        }

        graph->adjacencyMatrix.matrix[ coords[0] ][ coords[1] ]++;
        //graph->vertexDegree[ coords[0] ]++;

        if( !graph->isUndirected )
        {
            //graph->adjacencyMatrix.matrix[ coords[1] ][ coords[0] ]++;
            //graph->vertexDegree[ coords[0] ]++;
        }
    }

    for( int i = 0; i < graph->adjacencyMatrix.numRows; i++ )
    {
        for( int j = 0; j < graph->adjacencyMatrix.numCols; j++ )
        {
            if( graph->adjacencyMatrix.matrix[i][j] )
            {
                graph->vertexDegree[i] += graph->adjacencyMatrix.matrix[i][j];
            }
        }
    }

    /*for(int i = 0; i < graph->numVertices; i++)
    {
        printf("Degree of Vertex %i: %i\n", i+1, graph->vertexDegree[i]);
    }*/

    //graph->isUndirected = IsUndirectedByMatrix( graph );

}

/*/ Checks to see if there are any vertices left out of the edges
static inline int IsConnectedGraph( Graph* graph )
{
    int isolatedVertex[graph->adjacencyMatrix.numRows];

    // Check if every vertex is contained within at least one edge
    for( int i = 0; i < graph->adjacencyMatrix.numRows; i++ )
    {
        // Assume the vertex is isolated
        isolatedVertex[i] = 1;

        for( int j = 0; j < graph->adjacencyMatrix.numCols; j++ )
        {
            // If the entry is nonzero, then the vertex that row represents is not isolated
            if( graph->adjacencyMatrix.matrix[i][j] )// || graph->adjacencyMatrix.matrix[j][i] )
            {
                // The vertex is not isolated
                isolatedVertex[i] = 0;
                break; // Continue in outter loop
            }
        }

        // If that vertex is still considered isolated, then it is isolated and the graph is not complete
        if( isolatedVertex[i] )
        {
            return 0;
        }
    }

    return 1;
}*/



// Checks to see if there are any vertices left out of the edges
static inline int IsConnectedGraph( Graph* graph )
{
    int isolatedVertex[graph->numEdges];

    for( int i = 0; i < graph->numVertices; i++ )
    {
        // Assume isolated
        isolatedVertex[i] = 1;

        for( int j = 0; j < graph->numEdges; j++ )
        {
            if( ( graph->edges[j][0] == graph->vertices[i] ) || ( graph->edges[j][1] == graph->vertices[i] ) )
            {
                isolatedVertex[i] = 0;
            }
        }

        if( isolatedVertex[i] )
        {
            return 0;
        }
    }

    return 1;
}





static inline int GetSymmetricEdge( Graph* graph, char c1, char c2 )
{
    int result = -1;

    for( int i = 0; i < graph->numEdges; i++ )
    {
        if( graph->edges[i][0] == c2 && graph->edges[i][1] == c1 )
        {
            result = i;
        }
    }
    return result;
}

static inline int GetVertexDegree( Graph* graph, char vertex )
{
    int vertexDegree = 0;

    for( int i = 0; i < graph->numEdges; i++ )
    {
        if( graph->edges[i][0] == vertex )
        {
            vertexDegree++;
        }
    }

    return vertexDegree;
}


static inline int GetVertexInDegree( Graph* graph, char vertex )
{
    int inDegree = 0;

    for( int i = 0; i < graph->numEdges; i++ )
    {
        // Check terminal vertices to determine in degree
        if( graph->edges[i][1] == vertex )
        {
            inDegree++;
        }
    }

    return inDegree;
}

static inline int GetVertexOutDegree( Graph* graph, char vertex )
{
    int outDegree = 0;

    for( int i = 0; i < graph->numEdges; i++ )
    {
        // Check initial vertices to determine out degree
        if( graph->edges[i][0] == vertex )
        {
            outDegree++;
        }
    }

    return outDegree;
}


static inline int IsDeadEnd( Graph* graph, int vertex )
{
    char toCheck = graph->edges[vertex][1];

    int numEdges = 0;

    for( int i = 0; i < graph->numEdges; i++ )
    {
        // Find the vertex we want to check
        if( graph->edges[i][0] == toCheck )
        {
            numEdges++;
        }

        if( numEdges > 1 )
        {
            return 0;
        }
    }
    return 1;
}

/*static inline int IsDeadEndDirected( Graph* graph, int vertex )
{
    char toCheck = graph->edges[vertex][1];

    int numEdges = 0;

    for( int i = 0; i < graph->numEdges; i++ )
    {
        // Find the vertex we want to check
        if( graph->edges[i][0] == toCheck )
        {
            numEdges++;
        }

        if( numEdges > 0 )
        {
            return 0;
        }
    }
    return 1;
}*/

static inline int IsDeadEndDirected( Graph* graph, int vertex )
{
    char toCheck = graph->edges[vertex][1];

    /*char otherEnd;

    for( int i = 0; i < graph->numEdges; i++ )
    {
        if( graph->edges[i][0] == toCheck )
        {
            otherEnd = graph->edges[i][1];
            break;
        }
    }*/

    int result = 0;



    if( GetVertexOutDegree( graph, toCheck ) == 0 )
    {
        result = 1;
    }

    /*if( GetVertexOutDegree( graph, toCheck ) > 1 && GetVertexInDegree( graph, toCheck ) < 2 )
    {
        result = 1;
    }*/
    //else if( GetVertexOutDegree( graph, vertex ) == 1 &&  )

    return result;
}

static inline int IsNotDumb( Graph* graph, int vertex )
{
    char toCheck = graph->edges[vertex][1];

    for( int i = 0; i < graph->numEdges; i++ )
    {
        if( graph->edges[i][0] == toCheck )
        {
            for( int j = i; j < graph->numEdges; j++ )
            {
                if( graph->edges[j][0] == toCheck )
                {

                }
            }
        }
    }
}

void GetEuler( Graph* graph )
{
    // TODO: BAD
    char* result = (char*) malloc( sizeof(char) * 100 );
    int stackTop = 0;

    int lastStart = 0;

    if( graph->eulerType == E_CIRCUIT )
    {
        result[stackTop] = graph->edges[0][0];
    }
    else
    {
        if( graph->isUndirected )
        {
            for( int i = 0; i < graph->numVertices; i++ )
            {
                // If an odd vertex
                if( graph->vertexDegree[i] % 2 )
                {
                    char vertex = i + 'a';
                    result[stackTop] = vertex;
                    break;
                }
            }
        }
        else
        {
            int inDegree[graph->numVertices];
            int outDegree[graph->numVertices];

            for( int i = 0; i < graph->adjacencyMatrix.numRows; i++ )
            {
                outDegree[i] = 0;
                for( int j = 0; j < graph->adjacencyMatrix.numCols; j++ )
                {
                    if( graph->adjacencyMatrix.matrix[i][j] )
                    {
                        outDegree[i] += graph->adjacencyMatrix.matrix[i][j];
                    }
                }
            }

            for( int i = 0; i < graph->numVertices; i++ )
            {
                inDegree[i] = 0;
                for( int j = 0; j < graph->numEdges; j++ )
                {
                    // Check terminal vertices to determine in degree
                    if( graph->edges[j][1] == graph->vertices[i] )
                    {
                        inDegree[i]++;
                    }
                }
            }

            for( int i = 0; i < graph->numVertices; i++ )
            {
                if( (outDegree[i] != inDegree[i]) && (outDegree[i] > inDegree[i]) )
                {
                    result[stackTop] = i + 'a';
                    lastStart = i;
                    break;
                }
            }
        }
    }
    //printf("%c", result[stackTop]);

    //printf("%c", result[stackTop]); exit(1);


    int numEdges = graph->numEdges;

    Stack prevIndexStack = CreateStack( INT, 100 );
    Stack prevCharStack = CreateStack( CHAR, 100 );


    char badVertex = '0';

    int edgeCount = 0;
    int stranded = 0;

    int ohNo = 0;

    Restart:
    // NOTE: You are witnessing catastrophe... IDK WHY THIS FU!%!!! THING IS HERE ... ;(
    //... But I put it here so IT'S STAYING... hopefully never will need it :D
    // TODO: Rethink life....
    // TODO: Don't have this.
    if( ohNo )
    {
        if( !graph->isUndirected )
        {
            int inDegree[graph->numVertices];
            int outDegree[graph->numVertices];

            for( int i = 0; i < graph->adjacencyMatrix.numRows; i++ )
            {
                outDegree[i] = 0;
                for( int j = 0; j < graph->adjacencyMatrix.numCols; j++ )
                {
                    if( graph->adjacencyMatrix.matrix[i][j] )
                    {
                        outDegree[i] += graph->adjacencyMatrix.matrix[i][j];
                    }
                }
            }

            for( int i = 0; i < graph->numVertices; i++ )
            {
                inDegree[i] = 0;
                for( int j = 0; j < graph->numEdges; j++ )
                {
                    // Check terminal vertices to determine in degree
                    if( graph->edges[j][1] == graph->vertices[i] )
                    {
                        inDegree[i]++;
                    }
                }
            }

            for( int i = 0; i < graph->numVertices; i++ )
            {
                if( (outDegree[i] != inDegree[i]) && i != lastStart )
                {
                    result[stackTop] = i + 'a';
                }
            }
        }
    }

    while( numEdges )
    {
        for( int i = 0; i < graph->numEdges; i++ )
        {
            edgeCount++;
            //printf("char: %c\n", graph->edges[i][0] );

//printf("top: %c\n", result[stackTop] );
//PrintEdges(graph); printf("\n");
            // Find an edge that starts with the vertex on top of our stack
            if( graph->edges[i][1] != badVertex && result[ stackTop ] == graph->edges[i][0] )
            {
                edgeCount = 0;
                badVertex = '0';
//PrintEdges(graph); printf("\n");
//printf("top: %c\n", result[stackTop] );
//printf("vertex Degree: %i", GetVertexDegree( graph, result[ stackTop ] ) );
//getchar();
                // Check to see if we either have more than 2 vertices left and it is undirected
                //or we have more than 1 vertex left and it is directed
                // This is to ensure we hit every vertex
                if( (numEdges > 2 && graph->isUndirected) || (numEdges > 1 && !graph->isUndirected) )
                {
                    if( !IsDeadEnd( graph, i )  && graph->isUndirected )
                    {
                        stackTop++;
                        result[ stackTop ] = graph->edges[i][1];
                    }
                    else if( !IsDeadEndDirected( graph, i ) && !graph->isUndirected )
                    {
                        stackTop++;
                        result[ stackTop ] = graph->edges[i][1];
                    }
                }
                else
                {
                    stackTop++;
                    result[ stackTop ] = graph->edges[i][1];
                }


                if( graph->isUndirected )
                {
                    int symIndex = GetSymmetricEdge( graph, graph->edges[i][0], graph->edges[i][1] );
                    Push_c( &prevCharStack, graph->edges[symIndex][0] );
                    Push_i( &prevIndexStack, symIndex );
                    graph->edges[symIndex][0] = '-';
                    numEdges--;
                }

                // Disregard this edge next time
                Push_c( &prevCharStack, graph->edges[i][0] );
                Push_i( &prevIndexStack, i );

                graph->edges[i][0] = '-';
                numEdges--;
            }
            else if( stranded )
            {
                //printf("STRANDED!\n");
                while( stranded )
                {
                    badVertex = result[stackTop];

                    stackTop--;
                    if(stackTop < 0)
                    { break; }

                    graph->edges[ Pop_i( &prevIndexStack ) ][0] = Pop_c( &prevCharStack );
                    numEdges++;

                    if( graph->isUndirected)
                    {
                        graph->edges[ Pop_i( &prevIndexStack ) ][0] = Pop_c( &prevCharStack );
                        numEdges++;
                    }

                    //PrintEdges(graph); printf("\n");

                    //printf("stackTop = %i, char = %c\n", stackTop, result[stackTop]);

                    int vertexDegree = GetVertexDegree( graph, result[ stackTop ] );
                    int vertexOutDegree = GetVertexOutDegree( graph, result[stackTop] );
                    //PrintEdges(graph); printf("\n");

                    //printf("vertexDegree: %i\n", vertexDegree);
                    //printf("vertexOutDegree: %i\n", vertexOutDegree);

                    getchar();

                    if( graph->isUndirected && GetVertexDegree( graph, result[ stackTop ] ) > 1 )
                    {
                        stranded = 0;
                    }
                    else if( !graph->isUndirected && GetVertexOutDegree( graph, result[stackTop] ) > 1 )
                    {
                        stranded = 0;
                    }
                }
                //printf("\tOKAY WE GOOD\n");
            }
        }

        if(stackTop < 0)
        { break; }

        if( edgeCount > numEdges + 1 )//+ 1 )
        {
            //printf("edgeCount > numEdges\n");
            edgeCount = 0;
            stranded = 1;
        }

        if(stackTop < 0)
        { break; ohNo = 1; }
    }

    if( ohNo )
    {
        int numEdges = graph->numEdges;

        DeleteStack( &prevIndexStack );
        DeleteStack( &prevCharStack);

        Stack prevIndexStack = CreateStack( INT, 100 );
        Stack prevCharStack = CreateStack( CHAR, 100 );


        char badVertex = '0';

        int edgeCount = 0;
        int stranded = 0;

        int ohNo = 0;

        goto Restart;
    }

    for( int i = 0; i < stackTop + 1; i++ )
    {
        //printf("%d ", i);
        printf("%c", result[i]);
        if( i < stackTop )
            printf("-");
    }
}


static inline int UndirectedTest( Graph* graph )
{
    int result = E_NONE;

    int numVertices = graph->numVertices;

    int numEvenVertices = 0;
    int numOddVertices = 0;

    /*if( !graph->isUndirected )
    {
        for( int i = 0; i < numVertices; i++ )
            graph->vertexDegree[i]++;
    }*/

    for( int i = 0; i < numVertices; i++ )
    {
        // 0 if vertexDegree at vertex i is even
        if( !(graph->vertexDegree[i] % 2) )
        {
            numEvenVertices++;
        }
    }

    // We have numVertices and numEvenVertices of them are even, so the rest must be odd.
    numOddVertices = numVertices - numEvenVertices;

    //printf("NumODD: %i\n", numOddVertices);
    //printf("NumEVEN: %i\n", numEvenVertices);


    // If every vertex has an even degree then there is a Euler Circuit
    if( numEvenVertices == numVertices )
    {
        graph->eulerType = E_CIRCUIT;
        result = E_CIRCUIT;
    }
    // If there are exactly 2 vertices with odd degree then there is a Euler Path
    else if( numOddVertices == 2 )
    {
        graph->eulerType = E_PATH;
        result = E_PATH;
    }

    return result;
}

static inline int DirectedTest( Graph* graph )
{
    int inDegree[graph->numVertices];
    int outDegree[graph->numVertices];

    int result = E_NONE;

    int emptyRow = 1;

    for( int i = 0; i < graph->adjacencyMatrix.numRows; i++ )
    {
        outDegree[i] = 0;
        for( int j = 0; j < graph->adjacencyMatrix.numCols; j++ )
        {
            if( graph->adjacencyMatrix.matrix[i][j] )
            {
                emptyRow = 0;
                outDegree[i] += graph->adjacencyMatrix.matrix[i][j];
            }
        }

        if( emptyRow )
        {
            return E_NONE;
        }
        emptyRow = 1;
    }

    for( int i = 0; i < graph->numVertices; i++ )
    {
        inDegree[i] = 0;
        for( int j = 0; j < graph->numEdges; j++ )
        {
            // Check terminal vertices to determine in degree
            if( graph->edges[j][1] == graph->vertices[i] )
            {
                inDegree[i]++;
            }
        }
    }

    int circuitFlag = 1;

    for( int i = 0; i < graph->numVertices; i++ )
    {
        if( inDegree[i] != outDegree[i] )
        {
            circuitFlag = 0;
        }
    }

    if( circuitFlag )
    {
        result = E_CIRCUIT;
    }
    else
    {
        int notSameDegree = 0;

        for( int i = 0; i < graph->numVertices; i++ )
        {
            if( inDegree[i] != outDegree[i] )
            {
                notSameDegree++;
            }

            if( notSameDegree > 2 )
            {
                result = E_NONE;
                break;
            }
        }

        if( notSameDegree == 2 )
            result = E_PATH;
    }

    graph->eulerType = result;

    return result;
}

int TestGraph( Graph* graph )
{
    // The return value indicating:
    // 2 = Euler Path
    // 1 = Euler Circuit
    // 0 = None
    int result = E_NONE;

    if( IsConnectedGraph(graph) )
    {
        if( graph->isUndirected )
            result = UndirectedTest( graph );
        else
            result = DirectedTest( graph );
    }

    return result;
}


/*
for( int i = 0; i < graph->numEdges; i++ )
{
    for( int j = 0; j < 2; j++ )
    {
        if( graph->edge[i])
        {

        }
    }
}
*/
